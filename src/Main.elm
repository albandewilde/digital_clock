module Main exposing (init)

-- Digital clock to show current time in your time zone.
--
-- Based on the example at:
--   https://guide.elm-lang.org/effects/time.html
--
-- See example of an analog clock:
--   https://elm-lang.org/examples/clock
--

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Task
import Time

import FormatTime exposing (..)



-- MAIN


main : Program () Model Msg
main =
  Browser.element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }



-- MODEL


init : () -> (Model, Cmd Msg)
init _ =
  ( Model Time.utc (Time.millisToPosix 0)
  , Task.perform AdjustTimeZone Time.here
  )



-- UPDATE


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Tick newTime ->
      ( { model | time = newTime }
      , Cmd.none
      )

    AdjustTimeZone newZone ->
      ( { model | zone = newZone }
      , Cmd.none
      )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
  Time.every 10 Tick



-- VIEW


view : Model -> Html Msg
view model =
  let
    h = hour model
    m = minute model
    sec = second model
    time = (h ++ ":" ++ m ++ ":" ++ sec)
  in
  div
    -- [ style "text-align" "center"
    [ style "position" "absolute"
    , style "top" "50%"
    , style "left" "50%"
    , style "transform" "translateX(-50%) translateY(-50%)"
    ] (timeToHtmlImg time)
