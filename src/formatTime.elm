module FormatTime exposing (Msg(..), Model, hour, minute, second, timeToHtmlImg)


import Html exposing (..)
import Html.Attributes exposing (..)
import Time


type Msg
  = Tick Time.Posix
  | AdjustTimeZone Time.Zone


type alias Model =
  { zone : Time.Zone
  , time : Time.Posix
  }


hour : Model -> String
hour model =
  twoDigit (String.fromInt (Time.toHour   model.zone model.time))


minute : Model -> String
minute model =
  twoDigit (String.fromInt (Time.toMinute   model.zone model.time))


second : Model -> String
second model =
  twoDigit (String.fromInt (Time.toSecond   model.zone model.time))


twoDigit : String -> String
twoDigit number =
  if String.length number < 2 then
    "0" ++ number
  else
    number



digitToHtmlImg : String -> Html msg
digitToHtmlImg digit =
  let
    name = case digit of
      "0" -> "zero"
      "1" -> "one"
      "2" -> "two"
      "3" -> "three"
      "4" -> "four"
      "5" -> "five"
      "6" -> "six"
      "7" -> "seven"
      "8" -> "eight"
      "9" -> "nine"
      ":" -> "colon"
      _ -> "unknown"
  in
  img
    [ src ("/image/" ++ name ++ ".png")
    , alt name
    , style "max-height" "10em"
    ] []


timeToHtmlImg : String -> List (Html msg)
timeToHtmlImg time =
  [ digitToHtmlImg (String.slice 0 1 time)
  , digitToHtmlImg (String.slice 1 2 time)
  , digitToHtmlImg (String.slice 2 3 time)
  , digitToHtmlImg (String.slice 3 4 time)
  , digitToHtmlImg (String.slice 4 5 time)
  , digitToHtmlImg (String.slice 5 6 time)
  , digitToHtmlImg (String.slice 6 7 time)
  , digitToHtmlImg (String.slice 7 8 time)
  ]
