build: clean
	elm make ./src/Main.elm --optimize --output=./out/main.js
	cp -r ./image ./out
	cp -r ./html/* ./out

clean:
	rm -r ./out/*
	touch ./out/.gitkeep
